To include these styles in project, add a line like the following (import path may vary):

```scss
@import "@gitlab/ui/scss/gitlab_ui";
```

To override variables, copy _variables.scss and import it _before_ gitlab_ui.
