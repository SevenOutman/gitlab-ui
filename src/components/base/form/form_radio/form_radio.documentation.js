import description from './form_radio.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-radio',
};
