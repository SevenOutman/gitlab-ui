# Dropdown

<!-- STORY -->

## Usage

> Note: This component is still being developed. Please use [`<gl-dropdown>`] instead.

The dropdown component offers a user multiple items or actions to choose from which are initially collapsed behind a button.
