import description from './table.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
  bootstrapComponent: 'b-table',
};
