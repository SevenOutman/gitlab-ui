import DatepickerBasicExample from './datepicker.basic.example.vue';

export default [
  {
    name: 'Datepicker',
    items: [
      {
        id: 'basic-date-picker',
        name: 'Basic date picker',
        component: DatepickerBasicExample,
      },
    ],
  },
];
