import SprintfBasicExample from './sprintf.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'sprintf-basic',
        name: 'Basic',
        description: 'Basic sprintf',
        component: SprintfBasicExample,
      },
    ],
  },
];
