import description from './empty_state.md';
import examples from './examples';

export default {
  description,
  examples,
};
